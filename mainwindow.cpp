#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "matrixutil.hpp"


#include <QImage>
#include <QDebug>
#include <boost/numeric/ublas/matrix.hpp>



#include <iostream>
#include <vector>
#include <set>
#include <algorithm>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    FSupdateButtonState();
    //bestOfAll.bestFischer = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDatabaseInfo()
{
    ui->FScomboBox->clear();
    for(unsigned int i=1; i<=database.getNoFeatures(); ++i)
        ui->FScomboBox->addItem(QString::number(i));

    ui->FStextBrowserDatabaseInfo->setText("noClass: " +  QString::number(database.getNoClass()));
    ui->FStextBrowserDatabaseInfo->append("noObjects: "  +  QString::number(database.getNoObjects()));
    ui->FStextBrowserDatabaseInfo->append("noFeatures: "  +  QString::number(database.getNoFeatures()));

    ui->CcomboBoxClassifiers->addItem("NN");
    ui->CcomboBoxClassifiers->addItem("kNN");
    //ui->CcomboBoxClassifiers->addItem("kNM");
    ui->CcomboBoxClassifiers->addItem("NM");

    for (int k=1; k<database.getNoObjects(); k=k+2) {
        ui->CcomboBoxK->addItem(QString::number(k));
     }

}

void MainWindow::FSupdateButtonState(void)
{
    if(database.getNoObjects()==0)
    {
        FSsetButtonState(false);
    }
    else
        FSsetButtonState(true);

}


void MainWindow::FSsetButtonState(bool state)
{
   ui->FScomboBox->setEnabled(state);
   ui->FSpushButtonCompute->setEnabled(state);
   ui->FSpushButtonSaveFile->setEnabled(state);
   ui->FSradioButtonFisher->setEnabled(state);
   ui->FSradioButtonSFS->setEnabled(state);
}

void MainWindow::on_FSpushButtonOpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open TextFile"), "", tr("Texts Files (*.txt)"));

    if ( !database.load(fileName.toStdString()) )
        QMessageBox::warning(this, "Warning", "File corrupted !!!");
    else
        QMessageBox::information(this, fileName, "File loaded !!!");

    FSupdateButtonState();
    updateDatabaseInfo();
}

void MainWindow::on_FSpushButtonCompute_clicked()
{
    bestOfAll.bestFischer = 0;

    int dimension = ui->FScomboBox->currentText().toInt();


    if( ui->FSradioButtonFisher ->isChecked())
    {

        if (dimension == 1 && database.getNoClass() == 2) {
                int max_ind = singleFischer();
                ui->FStextBrowserDatabaseInfo->append("max_ind: "  +  QString::number(max_ind) + " " + QString::number(bestOfAll.bestFischer));
        }

        else if (dimension>1 && database.getNoClass() == 2) {

            std::set<int> featureIndicesAll;
            for(int i=0;i<database.getNoFeatures();i++) {featureIndicesAll.insert(i);}
            std::vector<int> featureIndicesChosen;

            FischerCombinations(featureIndicesChosen,featureIndicesAll,featureIndicesAll.begin(),dimension);

            ui->FStextBrowserDatabaseInfo->append("max_inds: ");
            for (int i = 0; i < dimension; ++i) {
                ui->FStextBrowserDatabaseInfo->append(QString::number(bestOfAll.bestIndices[i]) + ", ");
            }
            ui->FStextBrowserDatabaseInfo->append("Fischer: " + QString::number(bestOfAll.bestFischer));
        }
    }
    else if( ui->FSradioButtonSFS ->isChecked()) {

        std::set<int> featureIndicesAll;
        for(int i=0;i<database.getNoFeatures();i++) {featureIndicesAll.insert(i);}
        std::vector<int> featureIndicesChosen;
        int max_ind_single = singleFischer();
        featureIndicesChosen.push_back(max_ind_single);
        std::copy(featureIndicesChosen.begin(), featureIndicesChosen.end(), back_inserter(bestOfAll.bestIndices));

        SFSCombinations(featureIndicesChosen, featureIndicesAll, featureIndicesAll.begin(),dimension-1);

        ui->FStextBrowserDatabaseInfo->append("max_inds: ");
        for (int i = 0; i < bestOfAll.bestIndices.size(); ++i) {
            ui->FStextBrowserDatabaseInfo->append(QString::number(bestOfAll.bestIndices[i]) + ", ");
        }
        ui->FStextBrowserDatabaseInfo->append("Fischer: " + QString::number(bestOfAll.bestFischer));

    }

    if (bestOfAll.bestIndices.size() > 0) {
        ui->tabC->setEnabled(true);
    }

}



void MainWindow::on_FSpushButtonSaveFile_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
    tr("Open TextFile"), "D:\\Users\\Krzysiu\\Documents\\Visual Studio 2015\\Projects\\SMPD\\SMPD\\Debug\\", tr("Texts Files (*.txt)"));

        QMessageBox::information(this, "My File", fileName);
        database.save(fileName.toStdString());
}

void MainWindow::on_PpushButtonSelectFolder_clicked()
{
}

void MainWindow::on_CpushButtonOpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open TextFile"), "", tr("Texts Files (*.txt)"));

    if ( !database.load(fileName.toStdString()) )
        QMessageBox::warning(this, "Warning", "File corrupted !!!");
    else
        QMessageBox::information(this, fileName, "File loaded !!!");

    FSupdateButtonState();
    updateDatabaseInfo();
}

void MainWindow::on_CpushButtonSaveFile_clicked()
{

}

void MainWindow::on_CpushButtonTrain_clicked()
{
   int percentForTest = ui->CplainTextEditTrainingPart->toPlainText().toInt();

   trainObjects.clear();
   testObjects.clear();
   trainObjectsVectors.clear();
   testObjectsVectors.clear();
   afterShuffle.clear();

   ui->CtextBrowser->append("Number of features: "+QString::number(bestOfAll.bestIndices.size()));



   if(percentForTest>0  && percentForTest<100){
           ui->CtextBrowser->append("%: "+QString::number(percentForTest));
           int trainingPart=(database.getObjects().size()*percentForTest)/100;
       for(int i=0; i<database.getObjects().size(); ++i){
            afterShuffle.push_back(database.getObjects()[i]);
       }
       std::random_shuffle(afterShuffle.begin(), afterShuffle.end());

       int testingPart = database.getObjects().size() - trainingPart;

       for (int j=0; j<afterShuffle.size(); j=j+testingPart) {
           trainObjects.clear();
           testObjects.clear();

           for(int i=0; i<afterShuffle.size(); ++i)
           {
               if(i>=j && i<j+testingPart)
                   testObjects.push_back(afterShuffle[i]);
               else
                   trainObjects.push_back(afterShuffle[i]);
            }
           trainObjectsVectors.push_back(trainObjects);
           testObjectsVectors.push_back(testObjects);
       }

   }else{
       ui->CtextBrowser->append("Error!!! Training part must be between 0% and 100%.");
   }
   ui->CtextBrowser->append("trainSize: "+QString::number(trainObjects.size()));
   ui->CtextBrowser->append("testSize: "+QString::number(testObjects.size()));

}

void MainWindow::on_CpushButtonExecute_clicked()
{
    accuracyVector.clear();

    for (int i=0; i<trainObjectsVectors.size(); i++) {
        trainObjects = trainObjectsVectors[i];
        testObjects = testObjectsVectors[i];
        std::cout << i << " COUNT" << std::endl;
        countClassifiers();
    }

    double totalAccuracy = 0;
    for(auto &acc : accuracyVector) {
        totalAccuracy += acc;
    }


    totalAccuracy /= accuracyVector.size();
    ui->CtextBrowser->append("Total accuracy: "+QString::number(totalAccuracy)+"%.");




}

int MainWindow::singleFischer() {
    double tmp;
    int max_ind = -1;

    //std::map<std::string, int> classNames = database.getClassNames();
    for (uint i = 0; i < database.getNoFeatures(); ++i)
    {
        std::map<std::string, double> classAverages;
        std::map<std::string, double> classStds;

        for (auto const &ob : database.getObjects())
        {
            classAverages[ob.getClassName()] += ob.getFeatures()[i];
            classStds[ob.getClassName()] += ob.getFeatures()[i] * ob.getFeatures()[i];
        }

        std::for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it)
        {
            classAverages[it.first] /= it.second;
            classStds[it.first] = std::sqrt(classStds[it.first] / it.second - classAverages[it.first] * classAverages[it.first]);
        }
        );

        tmp = std::abs(classAverages[ database.getClassNames()[0] ] - classAverages[database.getClassNames()[1]]) / (classStds[database.getClassNames()[0]] + classStds[database.getClassNames()[1]]);

        if (tmp > bestOfAll.bestFischer)
        {
            bestOfAll.bestFischer = tmp;
            max_ind = i;
        }
    }
    return max_ind;
}


void MainWindow::FischerCombinations(std::vector<int>& featureIndicesChosen,std::set<int>& featureIndicesAll, std::set<int>::iterator currentValue, int numbersLeft)
{
    if(numbersLeft>0) {
        for(std::set<int>::iterator it=currentValue; it!=featureIndicesAll.end(); it++) {
            featureIndicesChosen.push_back(*it);
            std::set<int>::iterator next = it;
            next++;
            FischerCombinations(featureIndicesChosen,featureIndicesAll,next,numbersLeft-1);

            featureIndicesChosen.pop_back();

        }
    }
    else {
        NDFischer(featureIndicesChosen);
    }
}

void MainWindow::SFSCombinations(std::vector<int> featureIndicesChosen,std::set<int>& featureIndicesAll, std::set<int>::iterator currentValue, int numbersLeft)
{
    std::cout << "Entered SFScombinations with " << numbersLeft << " numbers left" << std::endl;
    std::cout << "Size of bestOfAll.bestIndices: " << bestOfAll.bestIndices.size() << std::endl;

    if(numbersLeft>0) {
        bestOfAll.bestFischer = 0;
        for(std::set<int>::iterator it=featureIndicesAll.begin(); it!=featureIndicesAll.end(); it++) {//indices all - ostatni to 63

            // do ND Fischer only when the index is not in the alrady chosen indices list
            int t = std::count(bestOfAll.bestIndices.begin(),bestOfAll.bestIndices.end(), *it);

            if (t>0) {
                std::cout <<  "item repeated" << std::endl;
            }
            else {
                featureIndicesChosen.push_back(*it);

                for (int i=0; i<featureIndicesChosen.size(); i++) {
                    std::cout << featureIndicesChosen[i] << ", ";
                }
                NDFischer(featureIndicesChosen);
                featureIndicesChosen.pop_back();
            }
        }

        std::cout << "After this loop: current best Fischer: " << bestOfAll.bestFischer << ", current bestOfAll.bestIndices: " ;
        for (int c=0; c<bestOfAll.bestIndices.size(); c++) {std::cout << bestOfAll.bestIndices[c] << ", " ;}
        std::cout << std::endl;
        SFSCombinations(bestOfAll.bestIndices, featureIndicesAll, featureIndicesAll.begin(),numbersLeft-1);
    }
}

void MainWindow::NDFischer(std::vector<int>& featureIndicesChosen)
{
    std::cout << "dimfIC: " << featureIndicesChosen.size()<< ", ";
    int dimension = featureIndicesChosen.size();

    std::map<std::string, boost::numeric::ublas::matrix<double>> classAverages;
    std::map<std::string, boost::numeric::ublas::matrix<double>> classFeatures;
    std::map<std::string, boost::numeric::ublas::matrix<double>> classFeaturesCentered;
    std::map<std::string, boost::numeric::ublas::matrix<double>> classScatterMatrix;
    std::map<std::string, double> classDets;
    double detSum = 0;

    for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it) {

        classAverages[it.first].resize(1, dimension, false);
        classFeatures[it.first].resize(dimension, it.second, false);
        classFeaturesCentered[it.first].resize(dimension, it.second, false);
        classScatterMatrix[it.first].resize(dimension, dimension, false);

        classAverages[it.first].clear();
        classFeatures[it.first].clear();
        classFeaturesCentered[it.first].clear();
        classScatterMatrix[it.first].clear();
    });

   std::map<std::string, int> objectNum;


   for (auto const &ob : database.getObjects()) {
        for (int i = 0; i < dimension; ++i) {

            classFeatures[ob.getClassName()](i, objectNum[ob.getClassName()]) = ob.getFeatures()[featureIndicesChosen[i]];
            classAverages[ob.getClassName()](0, i) += ob.getFeatures()[featureIndicesChosen[i]];


        }
        objectNum[ob.getClassName()]++;

   }

   std::for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it) {
       for (int i = 0; i < dimension; ++i) {
           classAverages[it.first](0,i) /= it.second;
       }
   }
   );

   double diffNorm = 0;

   for (int i = 0; i < classAverages[database.getClassNames()[0]].size2() ; ++i) {
       diffNorm += (classAverages[database.getClassNames()[1]](0, i) - classAverages[database.getClassNames()[0]](0, i)) *
               (classAverages[database.getClassNames()[1]](0, i) - classAverages[database.getClassNames()[0]](0, i));

   }


   std::for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it) {//dla każdej klasy - it.first
       for (int i = 0; i < dimension; ++i) {
           for (int j = 0; j < it.second; ++j) {
               classFeaturesCentered[it.first](i, j) = classFeatures[it.first](i, j) - classAverages[it.first](0, i);

           }
       }
       classScatterMatrix[it.first] = boost::numeric::ublas::prod(classFeaturesCentered[it.first], boost::numeric::ublas::trans(classFeaturesCentered[it.first])) * 1/it.second;

       classDets[it.first] = determinant(classScatterMatrix[it.first]);
       detSum += classDets[it.first];
   });


   double tmpFischer = sqrt(diffNorm)/detSum;
   std::cout << "tmpFischer: " << tmpFischer;

   if (tmpFischer > bestOfAll.bestFischer) {
       for (int c=0; c<featureIndicesChosen.size(); c++) {std::cout << featureIndicesChosen[c] << ", " ;}
       bestOfAll.bestFischer = tmpFischer;
       bestOfAll.bestIndices.clear();
       for (int c=0; c<featureIndicesChosen.size(); c++){
           bestOfAll.bestIndices.push_back(featureIndicesChosen[c]);
       }
   }

   std::cout << ", current best Fischer: " << bestOfAll.bestFischer << ", current bestOfAll.bestIndices: " ;
   for (int c=0; c<bestOfAll.bestIndices.size(); c++) {std::cout << bestOfAll.bestIndices[c] << ", " ;}
   std::cout << std::endl;
}

void MainWindow::countClassifiers()
{
    bestOfClass.bestDiff.clear();
    bestOfClass.bestClass.clear();
    int features = database.getNoFeatures();
    std::string chosenOne = (ui-> CcomboBoxClassifiers->itemText( ui-> CcomboBoxClassifiers-> currentIndex())).toStdString();
    int passed = 0;


    if(chosenOne=="NN"){

        for(auto &testSample : testObjects)
        {
            bestOfClass.bestDiff.clear();
            bestOfClass.bestClass.clear();

            bestOfClass.bestDiff.push_back(std::numeric_limits<double>::max());
            for(auto &trainSample : trainObjects)
            {
                double diff=0;
                for(auto &i : bestOfAll.bestIndices) {
                    diff+=std::pow((testSample.getFeatures()[i]-trainSample.getFeatures()[i]), 2);

                }
                diff = sqrt(diff);

                if (diff < bestOfClass.bestDiff[0])
                {
                    bestOfClass.bestDiff.clear();
                    bestOfClass.bestDiff.push_back(diff);
                    bestOfClass.bestClass.clear();
                    bestOfClass.bestClass.push_back(trainSample.getClassName());
                }
            }
            if(bestOfClass.bestClass[0]==testSample.getClassName()) ++passed;

        }
    }else if(chosenOne=="kNN"){
        int k =(ui-> CcomboBoxK->itemText( ui-> CcomboBoxK-> currentIndex())).toInt();
        std::vector<std::pair<double, std::string>> allDiffs;

        for(auto &testSample : testObjects)
        {
            bestOfClass.bestClass.clear();
            bestOfClass.bestDiff.clear();
            allDiffs.clear();
            for(auto &trainSample : trainObjects)
            {
                double diff=0;
                for(auto &i : bestOfAll.bestIndices) {
                    diff+=std::pow((testSample.getFeatures()[i]-trainSample.getFeatures()[i]), 2);
                }
                diff = sqrt(diff);
                allDiffs.push_back(std::make_pair(diff, trainSample.getClassName()));

            }

            sort(allDiffs.begin(), allDiffs.end(),[](std::pair<double, std::string> input1, std::pair<double, std::string> input2)
            {
                return input1.first < input2.first;//sorting order by diff
            });


            std::map<std::string, int> loopCounterForClass;

            for(int i=0; i<k; i++){ //auto &taken: allDiffs){
                    loopCounterForClass[allDiffs[i].second]++;

            }

            int tmpLoopCounter = 0;
            for (std::string className : database.getClassNames()) { //dla obu klas
                if (loopCounterForClass[className] > tmpLoopCounter) {
                    tmpLoopCounter = loopCounterForClass[className];

                    bestOfClass.bestClass.clear();
                    bestOfClass.bestClass.push_back(className);
                }
            }

            if(bestOfClass.bestClass[0]==testSample.getClassName()) ++passed;
        }

    }else if(chosenOne=="NM"){
        std::map<std::string, std::vector<double> > classAverages;
        for (std::string className : database.getClassNames()) {
            for(auto &i : bestOfAll.bestIndices) {
                classAverages[className].push_back(0);
            }
        }
        //std::cout << "size: " << bestOfAll.bestIndices.size();
        std::map<std::string, short int> loopCounterForClass;
        for(int i=0; i<bestOfAll.bestIndices.size(); i++) {
            for (const auto &trainSample : trainObjects){
                loopCounterForClass[trainSample.getClassName()] = loopCounterForClass[trainSample.getClassName()] + 1;
                classAverages[trainSample.getClassName()][i] += trainSample.getFeatures()[bestOfAll.bestIndices[i]];
            }

            for(const auto &it:classAverages){
                classAverages[it.first][i] /=loopCounterForClass[it.first];
            }

        }


        for(auto &testSample : testObjects)
        {
            bestOfClass.bestDiff.clear();
            bestOfClass.bestDiff.push_back(std::numeric_limits<double>::max());
            std::map<std::string, double> diff;

            for(int i=0; i<bestOfAll.bestIndices.size(); i++) {
                for(const auto &it:classAverages){

                    diff[it.first]+=std::pow((it.second[i] - testSample.getFeatures()[bestOfAll.bestIndices[i]]), 2);
                }
            }
            ;
            for(const auto &it:diff){
                diff[it.first]=sqrt(diff[it.first]);

                if (diff[it.first] < bestOfClass.bestDiff[0])
                {
                    bestOfClass.bestDiff.clear();
                    bestOfClass.bestDiff.push_back(diff[it.first]);
                    bestOfClass.bestClass.clear();
                    bestOfClass.bestClass.push_back(it.first);

                }
            }

            if(bestOfClass.bestClass[0]==testSample.getClassName()) ++passed;
        }


   }


    double passedDbl = double(passed);
    int all = testObjects.size();
    double accuracy = 100.0*passedDbl/(double(all));
    accuracyVector.push_back(accuracy);

    ui->CtextBrowser->append("correct: "+QString::number(passed));
    ui->CtextBrowser->append("all: "+QString::number(all));
    ui->CtextBrowser->append("accuracy: "+QString::number(accuracy)+"%.");
}
