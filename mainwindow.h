#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <fstream>
#include <QCheckBox>
#include <QFileDialog>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <set>


#include"database.h"

namespace Ui {
class MainWindow;
}

struct BestOfAll{double bestFischer; std::vector<int> bestIndices;};

struct BestOfClassifiers{std::vector<double> bestDiff; std::vector<std::string> bestClass;};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    bool loadFile(const std::string &fileName);
    void updateDatabaseInfo();
    void saveFile(const std::string &fileName);

    void FSupdateButtonState(void);
    void FSsetButtonState(bool state);



private slots:
    void on_FSpushButtonOpenFile_clicked();

    void on_FSpushButtonCompute_clicked();

    void on_FSpushButtonSaveFile_clicked();

    void on_PpushButtonSelectFolder_clicked();


    void on_CpushButtonOpenFile_clicked();

    void on_CpushButtonSaveFile_clicked();

    void on_CpushButtonTrain_clicked();

    void on_CpushButtonExecute_clicked();

    int singleFischer();
    void FischerCombinations(std::vector<int>& featureIndicesChosen,std::set<int>& featureIndicesAll, std::set<int>::iterator currentValue, int numbersLeft);
    void SFSCombinations(std::vector<int> featureIndicesChosen,std::set<int>& featureIndicesAll, std::set<int>::iterator currentValue, int numbersLeft);
    void NDFischer(std::vector<int>& featureIndicesChosen);
    void countClassifiers();

private:
    Ui::MainWindow *ui;

private:
     Database database;
     BestOfAll bestOfAll;
     BestOfClassifiers bestOfClass;
     std::vector<Object> afterShuffle;
     std::vector<Object> testObjects;
     std::vector<Object> trainObjects;
     std::vector<std::vector<Object> > trainObjectsVectors;
     std::vector<std::vector<Object> > testObjectsVectors;
     std::vector<double> accuracyVector;

};

#endif // MAINWINDOW_H
